require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Maitco Website"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About | Maitco Website"
  end

  test "should get contacts" do
    get :contacts
    assert_response :success
    assert_select "title", "Contacts | Maitco Website"
  end

  test "should get service" do
    get :service
    assert_response :success
    assert_select "title", "Services | Maitco Website"
  end
end
